﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Vuforia;


public class TrackUI : MonoBehaviour, ITrackableEventHandler
{
    public GameObject imageTarget;
    private TrackableBehaviour mTrackableBehaviour;
    private bool targetFound = false;
    public GameObject hide;
    string value;


    // Use this for initialization
    void Start () {
        mTrackableBehaviour = imageTarget.GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

    }

    // Update is called once per frame
    // Update é chamado uma vez por frame
    void Update()
    {
        if (targetFound)
        {
            hide.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit(); // exit app if back/esc button is pressed
                                    
            }
        }

        if (!targetFound)
        {
            hide.SetActive(false);
        }
    }

    void SelectionUI()
    {

    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            targetFound = true; //when target is found
                                // quando o destino é encontrado
        }
        else
        {
            targetFound = false; //when target is lost
                                 // quando o alvo é perdido
        }
    }



}
